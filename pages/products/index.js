import Link from "next/link";

export default function Products({ products }) {
    return (
        <>
            <h2>products</h2>
            {products.map((product) => (
            <div key={product.id}>
            <Link href={`/products/${product.id}`}><h5>{product.id}. {product.title}</h5></Link>
            <p>Price: ${product.price}</p>
            <p>Description: {product.description}</p>
            <p>Category: {product.category}</p>
            <p>Rating: {product.rating.rate} ({product.rating.count} reviews)</p>
            </div>
            ))}

        </>
    )
}

export async function getStaticProps() {
    const res = await fetch('https://fakestoreapi.com/products');
    const data = await res.json();

    return {
        props: {
            products: data,
        }
    }
}