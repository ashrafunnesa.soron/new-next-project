import Link from "next/link";
import { useRouter } from "next/router"

export default function SingleProducts({product}) {
    return (
        <>
            <h3>single product</h3>

            <div>
            <Link href={`/products/${product.id}`}><h5>{product.title}</h5></Link>
            <p>Price: ${product.price}</p>
            <p>Description: {product.description}</p>
            <p>Category: {product.category}</p>
            <img src={product.image} alt={product.title} />
            <p>Rating: {product.rating.rate} ({product.rating.count} reviews)</p>
            </div>
        </>
    )
}

export async function getStaticPaths() {
    const res =await fetch(`https://fakestoreapi.com/products`);
    const data = await res.json();

    const paths = data.map(post => {
        return {
            params: {
                slug: `${post.id}`
            }
        }
    });
    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps(context) {
    const {params} = context

    const res =await fetch(`https://fakestoreapi.com/products/${params.slug}`);
    const data = await res.json();

    return {
        props: {
            product: data,
        }
    }
}

